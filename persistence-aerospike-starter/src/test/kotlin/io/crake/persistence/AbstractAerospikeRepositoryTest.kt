package io.crake.persistence

import com.aerospike.client.AerospikeClient
import com.playtika.test.aerospike.AerospikeTestOperations
import io.crake.persistence.config.TestConfig
import io.crake.persistence.domain.Key
import io.crake.persistence.domain.Value
import io.crake.persistence.properties.RepositoryProperties
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import java.util.UUID

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = [TestConfig::class])
@ActiveProfiles("test")
open class AbstractAerospikeRepositoryTest {

    @Autowired
    lateinit var repository: Repository<Value, Key>

    @Autowired
    lateinit var repoProperties: RepositoryProperties

    @Autowired
    lateinit var aerospikeClient: AerospikeClient

    @Autowired
    lateinit var aerospikeTestOperations: AerospikeTestOperations

    val key = Key(UUID.randomUUID().toString())

    @AfterEach
    fun tearDown() {
        val aerospikeKey = com.aerospike.client.Key(repoProperties.namespace, repoProperties.setName, key.name)
        aerospikeClient.delete(null, aerospikeKey)
    }

    fun <T> run(func: suspend () -> T): Unit = runBlocking {
        func.invoke()
        return@runBlocking
    }
}
