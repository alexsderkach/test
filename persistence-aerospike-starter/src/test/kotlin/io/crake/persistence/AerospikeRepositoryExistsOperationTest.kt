package io.crake.persistence

import io.crake.persistence.domain.Value
import org.amshove.kluent.shouldBeFalse
import org.amshove.kluent.shouldBeTrue
import org.junit.jupiter.api.Test

class AerospikeRepositoryExistsOperationTest : AbstractAerospikeRepositoryTest() {

    @Test
    fun `given entity does not exist, when exists is called, should return false`() = run {
        // when
        val result = repository.exists(key)

        // then
        result.shouldBeFalse()
    }

    @Test
    fun `given entity exists, when exists is called, should return true`() = run {
        // given
        val existingValue = Value("1")
        repository.upsert(key, existingValue)

        // when
        val result = repository.exists(key)

        // then
        result.shouldBeTrue()
    }
}