package io.crake.persistence

import io.crake.persistence.domain.Value
import org.amshove.kluent.shouldBeNull
import org.joda.time.DateTime
import org.junit.jupiter.api.Test

class AerospikeRepositoryEntityTest : AbstractAerospikeRepositoryTest() {

    @Test
    fun `given entity exists, it should be removed after ttl has passed`() = run {
        // given
        val newValue = Value("1")
        repository.upsert(key, newValue)
        val ttlInMillis = repoProperties.ttl.toMillis().toInt()
        val aerospikeTimeLagInSeconds = 2

        // when
        aerospikeTestOperations.timeTravelTo(
            DateTime.now().plusMillis(ttlInMillis).plusSeconds(
                aerospikeTimeLagInSeconds
            )
        )

        // then
        repository.get(key).shouldBeNull()
    }
}