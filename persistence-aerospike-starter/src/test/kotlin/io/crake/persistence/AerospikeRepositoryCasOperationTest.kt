package io.crake.persistence

import io.crake.persistence.domain.Value
import org.amshove.kluent.shouldBeNull
import org.amshove.kluent.shouldEqual
import org.junit.jupiter.api.Test

class AerospikeRepositoryCasOperationTest : AbstractAerospikeRepositoryTest() {

    @Test
    fun `given entity does not exist, when cas is called, should provide null value`() = run {
        // when
        val newValue = Value("1")
        val result = repository.cas(key) {
            it.shouldBeNull()
            newValue
        }

        // then
        result.shouldEqual(newValue)
        repository.get(key).shouldEqual(newValue)
    }

    @Test
    fun `given entity exists, when cas is called, should provide existing value`() = run {
        // given
        val oldValue = Value("1")
        repository.upsert(key, oldValue)

        // when
        val newValue = Value("2")
        val result = repository.cas(key) {
            it.shouldEqual(oldValue)
            newValue
        }

        // then
        result.shouldEqual(newValue)
    }
}