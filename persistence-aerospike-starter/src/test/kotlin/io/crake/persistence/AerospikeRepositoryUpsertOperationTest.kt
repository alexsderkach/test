package io.crake.persistence

import io.crake.persistence.domain.Value
import org.amshove.kluent.shouldEqual
import org.junit.jupiter.api.Test

class AerospikeRepositoryUpsertOperationTest : AbstractAerospikeRepositoryTest() {

    @Test
    fun `given entity does not exist, when upsert is called, should create entity`() = run {
        // when
        val newValue = Value("1")
        val result = repository.upsert(key, newValue)

        // then
        result.shouldEqual(newValue)
        repository.get(key).shouldEqual(newValue)
    }

    @Test
    fun `given entity exists, when upsert is called, should replace entity`() = run {
        // given
        val existingValue = Value("1")
        repository.upsert(key, existingValue)

        // when
        val newValue = Value("2")
        val result = repository.upsert(key, newValue)

        // then
        result.shouldEqual(newValue)
        repository.get(key).shouldEqual(newValue)
    }
}