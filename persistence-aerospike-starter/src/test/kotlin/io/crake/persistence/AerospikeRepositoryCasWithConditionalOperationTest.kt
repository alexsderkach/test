package io.crake.persistence

import io.crake.persistence.domain.Value
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeNull
import org.amshove.kluent.shouldEqual
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class AerospikeRepositoryCasWithConditionalOperationTest : AbstractAerospikeRepositoryTest() {

    @Test
    fun `given entity does not exist, should provide null value and perform insert when persist requested`() = run {
        // when
        val newValue = Value("1")
        val result = repository.casWithConditional(key) {
            it.shouldBeNull()
            DoPersist(newValue)
        }

        // then
        result.shouldEqual(CasResult(null, newValue, CasStatus.MODIFIED))
        repository.get(key).shouldEqual(newValue)
    }

    @Test
    fun `given entity does not exist, should provide null value and throw exception when persist not requested`() =
        run {
            // when
            assertThrows<UnsupportedOperationException> {
                runBlocking {
                    repository.casWithConditional(key) {
                        it.shouldBeNull()
                        DoNothing()
                    }
                }
            }
        }

    @Test
    fun `given entity exists, should provide existing value and perform update when persist requested`() = run {
        // given
        val oldValue = Value("1")
        repository.upsert(key, oldValue)

        // when
        val newValue = Value("2")
        val result = repository.casWithConditional(key) {
            it.shouldEqual(oldValue)
            DoPersist(newValue)
        }

        // then
        result.shouldEqual(CasResult(oldValue, newValue, CasStatus.MODIFIED))
    }

    @Test
    fun `given entity exists, should provide existing value and not perform update when persist not requested`() = run {
        // given
        val oldValue = Value("1")
        repository.upsert(key, oldValue)

        // when
        val result = repository.casWithConditional(key) {
            it.shouldEqual(oldValue)
            DoNothing()
        }

        // then
        result.shouldEqual(CasResult(oldValue, oldValue, CasStatus.NOT_MODIFIED))
    }
}