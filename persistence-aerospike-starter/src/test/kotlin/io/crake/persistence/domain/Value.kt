package io.crake.persistence.domain

data class Value(val data: String)