package io.crake.persistence

import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class Application