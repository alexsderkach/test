package io.crake.persistence.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.playtika.test.aerospike.AerospikeTestOperationsAutoConfiguration
import io.crake.persistence.Repository
import io.crake.persistence.converter.SingleBinJsonAerospikeEntityConverter
import io.crake.persistence.domain.Key
import io.crake.persistence.domain.Value
import io.crake.persistence.properties.RepositoryProperties
import org.springframework.boot.autoconfigure.AutoConfigureBefore
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import

@TestConfiguration
@Import(AerospikeAutoConfiguration::class)
@AutoConfigureBefore(AerospikeTestOperationsAutoConfiguration::class)
open class TestConfig {

    @Bean
    open fun objectMapper() = ObjectMapper().apply {
        this.registerModule(KotlinModule())
    }

    @Bean
    @ConfigurationProperties("persistence.aerospike.repo.dummy")
    open fun repoProperties() = RepositoryProperties()

    @Bean
    open fun repository(repositoryFactory: io.crake.persistence.RepositoryFactory): Repository<Value, Key> {
        return repositoryFactory.repository(
            repoProperties(),
            SingleBinJsonAerospikeEntityConverter(objectMapper(), Value::class)
        ) { it.name }
    }
}