package io.crake.persistence

import io.crake.persistence.domain.Value
import org.amshove.kluent.shouldBeNull
import org.amshove.kluent.shouldEqual
import org.junit.jupiter.api.Test

class AerospikeRepositoryGetOperationTest : AbstractAerospikeRepositoryTest() {

    @Test
    fun `given entity does not exist, when get is called, should return null`() = run {
        // when
        val result = repository.get(key)

        // then
        result.shouldBeNull()
    }

    @Test
    fun `given entity exists, when get is called, should return existing entity`() = run {
        // given
        val existingValue = Value("1")
        repository.upsert(key, existingValue)

        // when
        val result = repository.get(key)

        // then
        result.shouldEqual(existingValue)
    }
}