package io.crake.persistence.config

import com.aerospike.client.AerospikeClient
import com.aerospike.client.Host
import com.aerospike.client.async.EventLoops
import com.aerospike.client.async.NettyEventLoops
import com.aerospike.client.cluster.Cluster
import com.aerospike.client.policy.ClientPolicy
import io.crake.persistence.properties.AerospikeProperties
import io.netty.channel.EventLoopGroup
import io.netty.channel.epoll.EpollEventLoopGroup
import io.netty.channel.nio.NioEventLoopGroup
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class AerospikeAutoConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "persistence.aerospike")
    open fun aerospikeProperties() = AerospikeProperties()

    @Bean
    @ConditionalOnExpression("T(org.apache.commons.lang3.SystemUtils).IS_OS_LINUX")
    open fun epollEventLoopGroup() = EpollEventLoopGroup()

    @Bean
    @ConditionalOnExpression("!T(org.apache.commons.lang3.SystemUtils).IS_OS_LINUX")
    open fun nioEventLoopGroup() = NioEventLoopGroup()

    @Bean
    open fun eventLoops(eventLoopGroup: EventLoopGroup): EventLoops = NettyEventLoops(eventLoopGroup)

    @Bean
    open fun clientPolicy(eventLoops: EventLoops) = ClientPolicy().apply {
        this.maxConnsPerNode = aerospikeProperties().maxConnectionsPerNode
        this.connPoolsPerNode = aerospikeProperties().connectionPoolsPerNode
        this.eventLoops = eventLoops
        this.failIfNotConnected = true
    }

    @Bean
    open fun aerospikeHosts() =
        aerospikeProperties().hosts
            .map { it.split(":") }
            .map { Host(it[0], it[1].toInt()) }.toTypedArray()

    @Bean(destroyMethod = "close")
    open fun cluster(clientPolicy: ClientPolicy, aerospikeHosts: Array<Host>) =
        Cluster(clientPolicy, aerospikeHosts)

    @Suppress("SpreadOperator")
    @Bean(destroyMethod = "close")
    open fun aerospikeClient(clientPolicy: ClientPolicy, aerospikeHosts: Array<Host>): AerospikeClient =
        AerospikeClient(clientPolicy, *aerospikeHosts)

    @Bean
    open fun repositoryFactory(client: AerospikeClient, eventLoops: EventLoops) =
        io.crake.persistence.RepositoryFactory(client, eventLoops)
}
