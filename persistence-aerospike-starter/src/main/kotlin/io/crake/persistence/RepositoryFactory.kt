package io.crake.persistence

import com.aerospike.client.AerospikeClient
import com.aerospike.client.async.EventLoops
import io.crake.persistence.converter.AerospikeEntityConverter
import io.crake.persistence.properties.RepositoryProperties

class RepositoryFactory(
    private val client: AerospikeClient,
    private val eventLoops: EventLoops
) {

    fun <T : Any, ID> repository(
        properties: RepositoryProperties,
        entityConverter: AerospikeEntityConverter<T>,
        keyFunction: (ID) -> String
    ): io.crake.persistence.Repository<T, ID> =
        io.crake.persistence.AerospikeRepository(
            client,
            eventLoops,
            keyFunction,
            properties,
            entityConverter
        )
}
