dependencies {
    api(project(":persistence-aerospike"))

    implementation("org.springframework.boot:spring-boot-autoconfigure")
    api("org.apache.commons:commons-lang3")
    api("io.netty:netty-all")

    testImplementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.7")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.cloud:spring-cloud-starter:2.0.2.RELEASE")
    testImplementation("org.testcontainers:testcontainers:1.10.2")
    testImplementation("com.playtika.testcontainers:embedded-aerospike:+") {
        exclude("com.github.docker-java", "docker-java")
    }
}