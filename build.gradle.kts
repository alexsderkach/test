plugins {
    id("io.crake.release")
    id("io.crake.kotlin-library") apply false
}

subprojects {
    apply {
        plugin("io.crake.kotlin-library")
    }
}