package io.crake.persistence

data class CasResult<T>(val old: T?, val new: T, val status: CasStatus)

enum class CasStatus {
    NOT_MODIFIED,
    MODIFIED
}
