package io.crake.persistence

interface Repository<T : Any, ID> {
    suspend fun get(id: ID): T?
    suspend fun upsert(id: ID, entity: T): T
    suspend fun delete(id: ID): Boolean
    suspend fun exists(id: ID): Boolean

    suspend fun cas(id: ID, updateFunction: suspend (T?) -> T): T
    suspend fun casWithResult(id: ID, updateFunction: suspend (T?) -> T): CasResult<T>
    suspend fun casWithConditional(id: ID, updateFunction: suspend (T?) -> CasConditional<T>): CasResult<T>
}
