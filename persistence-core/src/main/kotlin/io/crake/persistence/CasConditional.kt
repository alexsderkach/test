package io.crake.persistence

interface CasConditional<T>

class DoNothing<T> : CasConditional<T>
data class DoPersist<T>(val value: T) : CasConditional<T>
