package io.crake.persistence.listeners

import com.aerospike.client.AerospikeException
import com.aerospike.client.Key
import com.aerospike.client.listener.ExistsListener
import mu.KotlinLogging
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class DefaultExistsListener(private val continuation: Continuation<Boolean>, private val key: Key) : ExistsListener {

    private val log = KotlinLogging.logger {}

    override fun onSuccess(key: Key, exists: Boolean) {
        log.debug { "Aerospike end EXISTS - key: $key, exists: $exists" }
        continuation.resume(exists)
    }

    override fun onFailure(exception: AerospikeException) {
        log.error(exception) { "Aerospike end EXISTS failure - key: $key" }
        continuation.resumeWithException(exception)
    }
}
