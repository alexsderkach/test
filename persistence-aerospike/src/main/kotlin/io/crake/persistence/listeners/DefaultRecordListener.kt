package io.crake.persistence.listeners

import com.aerospike.client.AerospikeException
import com.aerospike.client.Key
import com.aerospike.client.Record
import com.aerospike.client.listener.RecordListener
import com.aerospike.client.query.KeyRecord
import mu.KotlinLogging
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class DefaultRecordListener(private val continuation: Continuation<KeyRecord>, private val key: Key) : RecordListener {
    private val log = KotlinLogging.logger {}

    override fun onSuccess(key: Key, record: Record?) {
        log.debug { "Aerospike end GET - key: $key, record: ${record ?: "<null>"}" }
        continuation.resume(KeyRecord(key, record))
    }

    override fun onFailure(exception: AerospikeException) {
        log.error(exception) { "Aerospike end GET failure - key: $key" }
        continuation.resumeWithException(exception)
    }
}
