package io.crake.persistence.listeners

import com.aerospike.client.AerospikeException
import com.aerospike.client.Key
import com.aerospike.client.listener.DeleteListener
import mu.KotlinLogging
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class DefaultDeleteListener(private val continuation: Continuation<KeyExisted>, private val key: Key) : DeleteListener {
    private val log = KotlinLogging.logger {}

    override fun onSuccess(key: Key, existed: Boolean) {
        log.debug { "Aerospike end DELETE - key: $key, existed: $existed" }
        continuation.resume(KeyExisted(key, existed))
    }

    override fun onFailure(exception: AerospikeException) {
        log.error(exception) { "Aerospike end DELETE failure - key: $key" }
        continuation.resumeWithException(exception)
    }
}

data class KeyExisted(val key: Key, val existed: Boolean)
