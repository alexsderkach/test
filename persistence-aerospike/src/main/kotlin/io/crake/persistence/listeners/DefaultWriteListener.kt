package io.crake.persistence.listeners

import com.aerospike.client.AerospikeException
import com.aerospike.client.Key
import com.aerospike.client.listener.WriteListener
import mu.KotlinLogging
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class DefaultWriteListener(private val continuation: Continuation<Key>, private val key: Key) : WriteListener {
    private val log = KotlinLogging.logger {}

    override fun onSuccess(key: Key) {
        log.debug { "Aerospike end WRITE - key: $key" }
        continuation.resume(key)
    }

    override fun onFailure(exception: AerospikeException) {
        log.error(exception) { "Aerospike end WRITE failure - key: $key" }
        continuation.resumeWithException(exception)
    }
}
