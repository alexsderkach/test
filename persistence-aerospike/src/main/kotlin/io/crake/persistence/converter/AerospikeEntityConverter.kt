package io.crake.persistence.converter

import com.aerospike.client.Bin
import com.aerospike.client.Record

interface AerospikeEntityConverter<T : Any> {
    fun toBins(entity: T): Array<Bin>
    fun fromRecord(record: Record): T
}
