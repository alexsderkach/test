package io.crake.persistence.converter

import com.aerospike.client.Bin
import com.aerospike.client.Record
import com.fasterxml.jackson.databind.ObjectMapper
import kotlin.reflect.KClass

class SingleBinJsonAerospikeEntityConverter<T : Any>(
    private val objectMapper: ObjectMapper,
    private val clazz: KClass<T>
) : AerospikeEntityConverter<T> {

    override fun toBins(entity: T): Array<Bin> =
        arrayOf(Bin(SINGLE_BIN_KEY, objectMapper.writeValueAsString(entity)))

    override fun fromRecord(record: Record): T =
        objectMapper.readValue(record.getString(SINGLE_BIN_KEY), clazz.java)

    companion object {
        private const val SINGLE_BIN_KEY = "v"
    }
}
