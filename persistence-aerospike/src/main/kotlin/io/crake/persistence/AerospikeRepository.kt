package io.crake.persistence

import com.aerospike.client.AerospikeClient
import com.aerospike.client.AerospikeException
import com.aerospike.client.Key
import com.aerospike.client.ResultCode
import com.aerospike.client.async.EventLoops
import com.aerospike.client.policy.GenerationPolicy
import com.aerospike.client.policy.Policy
import com.aerospike.client.policy.RecordExistsAction
import com.aerospike.client.policy.WritePolicy
import com.aerospike.client.query.KeyRecord
import io.crake.persistence.converter.AerospikeEntityConverter
import io.crake.persistence.listeners.DefaultDeleteListener
import io.crake.persistence.listeners.DefaultExistsListener
import io.crake.persistence.listeners.DefaultRecordListener
import io.crake.persistence.listeners.DefaultWriteListener
import io.crake.persistence.listeners.KeyExisted
import io.crake.persistence.properties.RepositoryProperties
import kotlinx.coroutines.delay
import mu.KotlinLogging
import java.util.concurrent.TimeUnit
import kotlin.coroutines.suspendCoroutine

@Suppress("TooManyFunctions")
class AerospikeRepository<T : Any, ID>(
    private val client: AerospikeClient,
    private val eventLoops: EventLoops,
    private val keyFunction: (ID) -> String,
    private val properties: RepositoryProperties,
    private val aerospikeEntityConverter: AerospikeEntityConverter<T>
) : Repository<T, ID> {

    private val log = KotlinLogging.logger {}

    companion object {
        val DEFAULT_RETRYABLE_CODES: List<Int> = listOf(
            ResultCode.ASYNC_QUEUE_FULL,
            ResultCode.SERVER_NOT_AVAILABLE,
            ResultCode.NO_MORE_CONNECTIONS,
            ResultCode.COMMAND_REJECTED,
            ResultCode.INVALID_NODE_ERROR,
            ResultCode.SERVER_ERROR,
            ResultCode.SERVER_MEM_ERROR,
            ResultCode.PARTITION_UNAVAILABLE,
            ResultCode.KEY_BUSY,
            ResultCode.CLUSTER_KEY_MISMATCH,
            ResultCode.DEVICE_OVERLOAD
        )
    }

    override suspend fun get(id: ID): T? = retry(DEFAULT_RETRYABLE_CODES) {
        doGet(id, properties.policies.readPolicy()).record?.let { aerospikeEntityConverter.fromRecord(it) }
    }

    override suspend fun exists(id: ID): Boolean = suspendCoroutine { cont ->
        val key = buildKey(id)
        log.debug { "Aerospike start EXISTS - key: $key" }
        client.exists(eventLoop(), DefaultExistsListener(cont, key), properties.policies.readPolicy(), key)
    }

    override suspend fun cas(id: ID, updateFunction: suspend (T?) -> T): T =
        casWithResult(id, updateFunction).new

    override suspend fun casWithResult(id: ID, updateFunction: suspend (T?) -> T): CasResult<T> =
        casWithConditional(id) { DoPersist(updateFunction.invoke(it)) }

    override suspend fun casWithConditional(id: ID, updateFunction: suspend (T?) -> CasConditional<T>): CasResult<T> =
        retry(
            DEFAULT_RETRYABLE_CODES.plus(
                listOf(
                    ResultCode.KEY_NOT_FOUND_ERROR,
                    ResultCode.GENERATION_ERROR,
                    ResultCode.KEY_EXISTS_ERROR
                )
            )
        ) {
            val keyRecord = doGet(id, properties.policies.readPolicy())
            return@retry if (keyRecord.record != null) {
                val toUpdate = aerospikeEntityConverter.fromRecord(keyRecord.record)
                val updated = updateFunction.invoke(toUpdate)
                when (updated) {
                    is DoPersist<T> -> {
                        val writePolicy = properties.policies.writePolicy().apply {
                            recordExistsAction = RecordExistsAction.REPLACE_ONLY
                            generationPolicy = GenerationPolicy.EXPECT_GEN_EQUAL
                            generation = keyRecord.record.generation
                        }
                        doWrite(id, updated.value, writePolicy)
                        CasResult(toUpdate, updated.value, CasStatus.MODIFIED)
                    }
                    else -> CasResult(toUpdate, toUpdate, CasStatus.NOT_MODIFIED)
                }
            } else {
                val created = updateFunction.invoke(null)
                when (created) {
                    is DoPersist<T> -> {
                        val writePolicy = properties.policies.writePolicy().apply {
                            recordExistsAction = RecordExistsAction.CREATE_ONLY
                        }
                        doWrite(id, created.value, writePolicy)
                        CasResult(null, created.value, CasStatus.MODIFIED)
                    }
                    else -> throw UnsupportedOperationException("Entity with id: $id expected to exist")
                }
            }
        }

    override suspend fun delete(id: ID): Boolean = retry(DEFAULT_RETRYABLE_CODES) {
        suspendCoroutine<KeyExisted> { cont ->
            val key = buildKey(id)
            log.debug { "Aerospike start DELETE - key: $key" }
            client.delete(
                eventLoops.next(),
                DefaultDeleteListener(cont, key),
                properties.policies.deletePolicy(),
                key
            )
        }.existed
    }

    override suspend fun upsert(id: ID, entity: T): T = retry(DEFAULT_RETRYABLE_CODES) {
        val writePolicy = properties.policies.writePolicy().apply {
            recordExistsAction = RecordExistsAction.REPLACE
        }
        doWrite(id, entity, writePolicy)
        entity
    }

    private fun eventLoop() = eventLoops.next()

    private fun buildKey(id: ID) = Key(properties.namespace, properties.setName, keyFunction.invoke(id))

    private suspend fun doGet(id: ID, readPolicy: Policy): KeyRecord = suspendCoroutine { cont ->
        val key = buildKey(id)
        log.debug { "Aerospike start GET - key: $key" }
        client.get(eventLoop(), DefaultRecordListener(cont, key), readPolicy, key)
    }

    @Suppress("SpreadOperator")
    private suspend fun doWrite(id: ID, entity: T, writePolicy: WritePolicy): Key = suspendCoroutine { cont ->
        val key = buildKey(id)
        writePolicy.expiration = TimeUnit.MILLISECONDS.toSeconds(properties.ttl.toMillis()).toInt()
        log.debug { "Aerospike start WRITE - key: $key" }
        client.put(
            eventLoop(),
            DefaultWriteListener(cont, key),
            writePolicy,
            key,
            *aerospikeEntityConverter.toBins(entity)
        )
    }

    private suspend fun <R> retry(
        retryableCodes: List<Int>,
        block: suspend () -> R
    ): R {
        with(properties.retry) {
            var currentDelay = initialDelay.toMillis()
            repeat(times - 1) {
                try {
                    return block()
                } catch (e: AerospikeException) {
                    if (retryableCodes.contains(e.resultCode)) {
                        delay(currentDelay)
                        currentDelay = (currentDelay * factor).toLong().coerceAtMost(maxDelay.toMillis())
                    } else {
                        throw e
                    }
                }
            }
            return block()
        }
    }
}
