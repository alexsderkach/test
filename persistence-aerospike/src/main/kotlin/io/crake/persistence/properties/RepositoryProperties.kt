package io.crake.persistence.properties

import org.jetbrains.annotations.NotNull
import java.time.Duration

class RepositoryProperties {
    @NotNull
    var namespace: String? = null
    @NotNull
    var setName: String? = null
    var ttl: Duration = Duration.ZERO
    var retry: RetryConfig = RetryConfig()
    var policies: RepositoryPolicies =
        RepositoryPolicies()
}

class RetryConfig {
    var times: Int = DEFAULT_RETRY_TIMES
    var initialDelay: Duration = DEFAULT_INITIAL_DELAY
    var maxDelay: Duration = DEFAULT_MAX_DELAY
    var factor: Double = DEFAULT_FACTOR

    companion object {
        val DEFAULT_RETRY_TIMES = 5
        val DEFAULT_INITIAL_DELAY = Duration.ofMillis(50)
        val DEFAULT_MAX_DELAY = Duration.ofMillis(200)
        val DEFAULT_FACTOR = 2.0
    }
}

class RepositoryPolicies {
    var read: PolicyProperties = PolicyProperties()
    var write: WritePolicyProperties =
        WritePolicyProperties()
    var delete: WritePolicyProperties =
        WritePolicyProperties()

    fun readPolicy() = read.toPolicy()
    fun writePolicy() = write.toWritePolicy()
    fun deletePolicy() = delete.toWritePolicy()
}
