package io.crake.persistence.properties

import com.aerospike.client.policy.ClientPolicy

class AerospikeProperties {
    var maxConnectionsPerNode = DEFAULT_CLIENT_POLICY.maxConnsPerNode
    var connectionPoolsPerNode = DEFAULT_CLIENT_POLICY.connPoolsPerNode
    var hosts: List<String> = listOf()

    companion object {
        val DEFAULT_CLIENT_POLICY = ClientPolicy()
    }
}
