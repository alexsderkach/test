package io.crake.persistence.properties

import com.aerospike.client.policy.CommitLevel
import com.aerospike.client.policy.ConsistencyLevel
import com.aerospike.client.policy.GenerationPolicy
import com.aerospike.client.policy.Policy
import com.aerospike.client.policy.Priority
import com.aerospike.client.policy.RecordExistsAction
import com.aerospike.client.policy.Replica
import com.aerospike.client.policy.WritePolicy

open class PolicyProperties {
    var priority: Priority = DEFAULT_POLICY.priority
    var consistencyLevel: ConsistencyLevel = DEFAULT_POLICY.consistencyLevel
    var replica: Replica = DEFAULT_POLICY.replica
    var socketTimeout: Int = DEFAULT_POLICY.socketTimeout
    var totalTimeout: Int = DEFAULT_POLICY.totalTimeout
    var timeoutDelay: Int = DEFAULT_POLICY.timeoutDelay
    var maxRetries: Int = DEFAULT_POLICY.maxRetries
    var sleepBetweenRetries: Int = DEFAULT_POLICY.sleepBetweenRetries
    var sendKey: Boolean = DEFAULT_POLICY.sendKey
    var linearizeRead: Boolean = DEFAULT_POLICY.linearizeRead

    fun toPolicy() = Policy().apply {
        this.priority = this@PolicyProperties.priority
        this.consistencyLevel = this@PolicyProperties.consistencyLevel
        this.replica = this@PolicyProperties.replica
        this.socketTimeout = this@PolicyProperties.socketTimeout
        this.totalTimeout = this@PolicyProperties.totalTimeout
        this.timeoutDelay = this@PolicyProperties.timeoutDelay
        this.maxRetries = this@PolicyProperties.maxRetries
        this.sleepBetweenRetries = this@PolicyProperties.sleepBetweenRetries
        this.sendKey = this@PolicyProperties.sendKey
        this.linearizeRead = this@PolicyProperties.linearizeRead
    }

    companion object {
        val DEFAULT_POLICY = Policy()
    }
}

open class WritePolicyProperties : PolicyProperties() {
    var recordExistsAction: RecordExistsAction = DEFAULT_WRITE_POLICY.recordExistsAction
    var generationPolicy: GenerationPolicy = DEFAULT_WRITE_POLICY.generationPolicy
    var commitLevel: CommitLevel = DEFAULT_WRITE_POLICY.commitLevel
    var generation: Int = DEFAULT_WRITE_POLICY.generation
    var expiration: Int = DEFAULT_WRITE_POLICY.expiration
    var respondAllOps: Boolean = DEFAULT_WRITE_POLICY.respondAllOps
    var durableDelete: Boolean = DEFAULT_WRITE_POLICY.durableDelete

    fun toWritePolicy() = WritePolicy(toPolicy()).apply {
        this.recordExistsAction = this@WritePolicyProperties.recordExistsAction
        this.generationPolicy = this@WritePolicyProperties.generationPolicy
        this.commitLevel = this@WritePolicyProperties.commitLevel
        this.generation = this@WritePolicyProperties.generation
        this.expiration = this@WritePolicyProperties.expiration
        this.respondAllOps = this@WritePolicyProperties.respondAllOps
        this.durableDelete = this@WritePolicyProperties.durableDelete
    }

    companion object {
        val DEFAULT_WRITE_POLICY = WritePolicy()
    }
}
