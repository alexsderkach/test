dependencies {
    api(project(":persistence-core"))

    implementation("com.fasterxml.jackson.core:jackson-databind")
    implementation("io.github.microutils:kotlin-logging:1.6.22")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.7")
    api("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.0.1")
    api("com.aerospike:aerospike-client:4.2.3")
}